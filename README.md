# Project Name

The Auth DB schema for Oracle, SQL Server and Maria DB/MySQL

## Installation

The statements are separated using (newline);(newline) separator or  (newline)/(newline) separator.

## Contributing

## Guidelines

* When naming something name it first on Oracle as it is the most restricted when it come to naming.
* When building a store procedure, build it first on MariaDB/Mysql as it is the most restricted when it come to store procedures.
* When building foreign key constraints build them first on SQL server as it is the most restricted when it come to foreign key constraints.

### Rules

1. Identifier names should be less than 30 characters in order to work with Oracle but should also apply for MySQL and SQL Server for consistency.
1. Use variables should not be used in MySQL Scripts as they need extra connection string parameter
1. All identifier names must be the same between vendors including foreign key names
1. All table names, store procedure names must be written in UPPER CASE because on Linux MariaDB/MySQL are *case sensitive*
1. ON DELETE CASCADE when to use:
   1. When a child table references a parent table. E.g. A table's Primary Key references USER table.
   1. It works with SQL Server :)

### Git

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

See [History](HISTORY.md)

## License

EUPL v1.1
