# HISTORY

## 1.0

*NOTE* MariaDB 10.1 or greater is required

### Tickets 1.0

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store

### Detailed changes 1.0

Initial release