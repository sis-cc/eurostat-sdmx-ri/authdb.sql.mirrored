-- delete all AUTHDB foreign key constraints and tables
begin
  DECLARE @sql NVARCHAR(MAX) = N'';

with msdb_tables as (
SELECT name FROM sys.tables where name in (
	'AUTH_DB_VERSION',
	'USER_PERMISSION',
	'MS_STORE_USER',
	'SRI_USER',
	'ACCESS_RULE_IMPLICIT',
	'ACCESS_RULE',
	'MS_STORE'
)),
constraints as (
select N'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
    + '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) + 
    ' DROP CONSTRAINT ' + QUOTENAME(name) + ';'  + CHAR(13) + CHAR(10) as alterDdl FROM sys.foreign_keys where OBJECT_NAME(parent_object_id) in (select name from msdb_tables) )

select @sql += alterDdl from (select alterDdl from constraints union all select N'DROP TABLE ' + QUOTENAME(name) + ';' + CHAR(13) + CHAR(10) from msdb_tables) a;

execute(@sql);
end
;
GO
;

-- delete all AUTHDB procedures
begin
DECLARE @sql NVARCHAR(MAX) = N'';
select @sql += N'DROP PROCEDURE ' + QUOTENAME(name) + ';' + CHAR(13) + CHAR(10) from sys.procedures where name in (
'INSERT_SRI_USER');

EXECUTE(@sql)
end
;

;
GO
;

