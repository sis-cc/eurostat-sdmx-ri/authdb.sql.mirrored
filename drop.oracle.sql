BEGIN
	FOR i IN (SELECT 'drop table "'|| table_name || '" CASCADE CONSTRAINTS' as droptable FROM user_tables WHERE table_name in (
	'AUTH_DB_VERSION',
	'USER_PERMISSION',
	'MS_STORE_USER',
	'SRI_USER',
	'ACCESS_RULE_IMPLICIT',
	'ACCESS_RULE',
	'MS_STORE'
	 )) LOOP
		dbms_output.put_line(i.droptable); 
		EXECUTE IMMEDIATE i.droptable;
	END LOOP;
END;
/

BEGIN
	FOR i IN (SELECT 'drop sequence '|| sequence_name as dropsequence from user_sequences where sequence_name in ('SRI_USER_ID_SEQ')) loop
	dbms_output.put_line(i.dropsequence);
	EXECUTE IMMEDIATE i.dropsequence;
	END LOOP;
END;
/

BEGIN
	FOR i IN (SELECT 'drop trigger '|| trigger_name as droptrigger from user_triggers where trigger_name in ('SET_SRI_USER_ID')) loop
	dbms_output.put_line(i.droptrigger);
	EXECUTE IMMEDIATE i.droptrigger;
	END LOOP;
END;
/

